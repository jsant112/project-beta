import React, { useState, useEffect } from 'react';

function SalesForm({fetchSales}) {
  const [automobile, setAutomobile] = useState('');
  const [automobiles, setAutomobiles] = useState([]);
  const [salesperson, setSalesPerson] = useState('');
  const [salespeople, setSalesPeople] = useState([]);
  const [customer, setCustomer] = useState('');
  const [customers, setCustomers] = useState([]);
  const [price, setPrice] = useState('');
  const [hasCreatedSale, setHasCreatedSale] = useState([])

  async function fetchAutomobiles() {
    const url = 'http://localhost:8100/api/automobiles/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  }

  async function fetchSalesPeople() {
    const url = 'http://localhost:8090/api/salespeople/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setSalesPeople(data.salespeople);
    }
  }

  async function fetchCustomers() {
    const url = 'http://localhost:8090/api/customers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }
  useEffect(() => {
    fetchAutomobiles();
    fetchSalesPeople();
    fetchCustomers();
    fetchSales();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      automobile,
      salesperson,
      customer,
      price,
    };

    const saleUrl = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(saleUrl, fetchConfig);

    if (response.ok) {
      setAutomobile("");
      setSalesPerson("");
      setCustomer("");
      setPrice("");
      setHasCreatedSale(true);

      const updateUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
      const updateData = {
      ...automobile,
      sold: true,

      }
    const updateOptions = {
      method: 'put',
      body: JSON.stringify(updateData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    await fetch(updateUrl, updateOptions);
    await fetchAutomobiles();
    await fetchSales();
    }
  }

  function handleAutomobileChange(event) {
    const { value } = event.target;
    setAutomobile(value);
  }

  function handleSalesPersonChange(event) {
    const { value } = event.target;
    setSalesPerson(value);
  }

  function handleCustomerChange(event) {
    const { value } = event.target;
    setCustomer(value);
  }

  function handlePriceChange(event) {
    const { value } = event.target;
    setPrice(value);
  }

  const unsoldAutomobiles = automobiles.filter((automobile) => !automobile.sold);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select value={automobile} onChange={handleAutomobileChange} placeholder="Automobile" name="automobile" id="automobile" className="form-select">
                <option value="">Choose an Automobile Vin...</option>
                {unsoldAutomobiles.map((automobile) => {
                  return (
                    <option key={automobile.vin} value={automobile.automobile}>{automobile.vin}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={salesperson} onChange={handleSalesPersonChange}  name="salesperson" id="salesperson" className="form-select">
                <option value="">Choose a Salesperson...</option>
                {salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.employee_id} value={salesperson.employee_id}>{`${salesperson.first_name} ${salesperson.last_name}`}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select value={customer} onChange={handleCustomerChange}  name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer...</option>
                {customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{`${customer.first_name} ${customer.last_name}`}</option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input value={price} onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
