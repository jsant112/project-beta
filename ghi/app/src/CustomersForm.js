import React, { useState } from 'react';

function CustomersForm({fetchCustomers}) {
    const [first_name, setFirst] = useState("");
    const [last_name, setLast] = useState("");
    const [phone_number, setPhoneNumber] = useState("");
    const [address, setAddress] = useState("");

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            phone_number,
            address,
        };
        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {

            setFirst("");
            setLast("");
            setPhoneNumber("");
            setAddress("");
            fetchCustomers();
        }
    }

    function handleFirstChange(event) {
        const { value } = event.target;
        setFirst(value);
    }

    function handleLastChange(event) {
        const { value } = event.target;
        setLast(value);
    }

    function handlePhoneNumberChange(event) {
      const { value } = event.target;
      setPhoneNumber(value);
  }
    function handleAddressChange(event) {
      const { value } = event.target;
      setAddress(value);
  }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer</h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input value={first_name} onChange={handleFirstChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                  <label htmlFor="first name">First Name...</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={last_name} onChange={handleLastChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                  <label htmlFor="last name">Last Name...</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={phone_number} onChange={handlePhoneNumberChange} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" />
                  <label htmlFor="phone number">Phone Number...</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={address} onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                  <label htmlFor="address">Address...</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

export default CustomersForm;
