import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from "react";
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersForm from "./ManufacturersForm";
import ManufacturersList from "./ManufacturersList";
import SalespeopleForm from "./SalespeopleForm";
import SalespeopleList from "./SalespeopleList";
import CustomersForm from "./CustomersForm";
import CustomersList from "./CustomersList";
import SalesForm from "./SalesForm";
import SalesList from "./SalesList";
import SalespersonHistory from "./SalespersonHistory";
import ModelsList from "./ModelsList";
import TechnicianForm from "./Services/CreateNewTechnician";
import TechnicianList from "./Services/TechnicianList";
import AppointmentForm from "./Services/CreateAppointment";
import AppointmentList from "./Services/ListAppointments";
import AppointmentHistoryList from "./Services/ServiceHistory";
import ModelsForm from './ModelsForm';
import AutomobileForm from './AutomobileForm';
import AutomobilesList from './AutomobileList';


function App() {
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [automobiles, setAutomobiles] = useState([])
  const [models, setModels] = useState([]);
  const [manufacturers, setManufacturers] = useState([]);
  const [techs, setTechs] = useState([]);
  const [appts, setAppts] = useState([]);
  // const [hist, setApptHistory] = useState([])

  async function fetchManufacturers() {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }
  async function fetchModels() {
    const url = "http://localhost:8100/api/models/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }
  async function fetchSalespeople() {
    const url = "http://localhost:8090/api/salespeople/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSalespeople(data.salespeople);
    }
  }
  async function fetchCustomers() {
    const url = "http://localhost:8090/api/customers/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }
  async function fetchSales() {
    const url = "http://localhost:8090/api/sales/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  }

  async function fetchAutomobiles() {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.automobiles);
    }
  }
  async function getTechs() {
    const response = await fetch('http://localhost:8080/api/technicians/')
    if (response.ok) {
      const data = await response.json();
      setTechs(data.technicians)
    }
    else {
      console.error('An error occurred fetching technician data')
    }
  };

  async function getAppts() {
    const response = await fetch('http://localhost:8080/api/appointments/')
    if (response.ok) {
      const data = await response.json();
      setAppts(data.appointments)
    }
    else {
      console.error('An error occurred fetching appointment data')
    }
  };
  // async function getApptHistory() {
  //   const response = await fetch('http://localhost:8080/api/appointments/')
  //   if (response.ok) {
  //     const data = await response.json();
  //     setApptHistory(data.appointments)
  //   }
  //   else {
  //     console.error('An error occurred fetching appointment history')
  //   }
  // }

  useEffect(() => {
    fetchManufacturers();
    fetchModels();
    fetchSalespeople();
    fetchCustomers();
    fetchSales();
    fetchAutomobiles();
    getTechs();
    getAppts();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="/automobiles/new/" element={<AutomobileForm />} />
          <Route path="/automobiles/" element={<AutomobilesList automobiles={automobiles} />} />


          <Route path="manufacturers">
            <Route path="new" element={<ManufacturersForm fetchManufacturers={fetchManufacturers} />} />
            <Route index element={<ManufacturersList manufacturers={manufacturers} />} />
          </Route>

          <Route path="models">
            <Route index element={<ModelsList models={models} />} />
            <Route path="new/" element={<ModelsForm manufacturers={manufacturers} fetchModels={fetchModels} />} />
          </Route>

          <Route path="salespeople">
            <Route path="new" element={<SalespeopleForm fetchSalespeople={fetchSalespeople} />} />
            <Route index element={<SalespeopleList salespeople={salespeople} />} />
          </Route>

          <Route path="customers">
            <Route path="new" element={<CustomersForm fetchCustomers={fetchCustomers} />} />
            <Route index element={<CustomersList customers={customers} />} />
          </Route>

          <Route path="sales">
            <Route path="new" element={<SalesForm fetchAutomobiles={fetchAutomobiles} fetchSales={fetchSales} />} />
            <Route index element={<SalesList sales={sales} />} />
            <Route path="history" element={<SalespersonHistory />} />
          </Route>
          <Route path="/technicians/">
            <Route index element={<TechnicianList techs={techs} />} />
            <Route path="create/" element={<TechnicianForm getTechs={getTechs} />} />
          </Route>
          <Route path="/appointments/">
            <Route index element={<AppointmentList appts={appts} getAppts={getAppts} />} />
            <Route path="create/" element={<AppointmentForm techs={techs} getAppts={getAppts} />} />
            <Route path="/appointments/history/" element={<AppointmentHistoryList />} />
          </Route>

        </Routes >
      </div >
    </BrowserRouter >
  );
}

export default App;
