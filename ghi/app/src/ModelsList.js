function ModelsList(props) {
  if (props.models === undefined) {
    return null;
}

  return (
    <>
    <h1> Vehicle Models </h1>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Model Name</th>
                <th>Manufacturer Name</th>
            </tr>
        </thead>
        <tbody>
          {props.models.map((model) => {
            return (
              <tr key={model.id}>
                <td>{model.name}</td>
                <td>{model.manufacturer.name}</td>
                <td>
                  <img src={model.picture_url} alt="name" style={{width: '100px', height: 'auto'}} ></img>
              </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}


export default ModelsList;
