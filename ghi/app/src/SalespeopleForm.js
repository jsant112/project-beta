import React, { useState } from 'react';

function SalespeopleForm({fetchSalespeople}) {
    const [first_name, setFirst] = useState("");
    const [last_name, setLast] = useState("");
    const [employee_id, setEmployeeId] = useState("");

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            first_name,
            last_name,
            employee_id,
        };
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {

            setFirst("");
            setLast("");
            setEmployeeId("");
            fetchSalespeople();
        }
    }

    function handleFirstChange(event) {
        const { value } = event.target;
        setFirst(value);
    }

    function handleLastChange(event) {
        const { value } = event.target;
        setLast(value);
    }

    function handleEmployeeIdChange(event) {
        const { value } = event.target;
        setEmployeeId(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Salesperson</h1>
              <form onSubmit={handleSubmit} id="create-salesperson-form">
                <div className="form-floating mb-3">
                  <input value={first_name} onChange={handleFirstChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                  <label htmlFor="first name">First Name...</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={last_name} onChange={handleLastChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                  <label htmlFor="last name">Last Name...</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={employee_id} onChange={handleEmployeeIdChange} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                  <label htmlFor="employee id">Employee Id...</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

export default SalespeopleForm;
