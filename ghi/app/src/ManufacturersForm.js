import React, { useState } from 'react';

function ManufacturersForm({fetchManufacturers}) {
    const [name, setName] = useState("");

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            name
        };
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {

            setName("");
            await fetchManufacturers();
        }
    }

    function handleNameChange(event) {
        const { value } = event.target;
        setName(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a Manufacturer</h1>
              <form onSubmit={handleSubmit} id="create-manufacturer-form">
                <div className="form-floating mb-3">
                  <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Manufacturer Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }

    export default ManufacturersForm;
