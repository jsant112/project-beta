# CarCar

Team:

* Person 1 - John Santoro = Service
* Person 2 - Yohan Pak = Sales

## How to Run this App

1. Fork and clone this repository with this command: git clone https://gitlab.com/jsant112/project-beta.git
2. When inside the correct directory in your terminal, enter the following commands:
    docker volume create beta-data
    docker-compose build
    docker-compose up
3. Wait for the following commands to finish inside your terminal, and then check your docker to see if you have 7 containers all running.
4. You should now be able to access http://localhost:3000/ in the address bar in your browser.

## Design

![Alt text](Excalidraw.png)

## Service microservice

For the services microservice, I have a total of 3 models, Technician. AutomobileVO, and Appointment. The Technician model outlines the requirements for someone to be a technician, Automobile contains the VIN and Sold status of a given car, and Appointment defines the parameters for creating an appointment, while creating and assigning different status', and using the Technician model.

## Sales microservice

The sales microservice contains four models: Salesperson, Customer, AutomobileVO, and Sale. For our AutomobileVO, we are using a poller to collect data from the Inventory API model Automobile. It polls for instances of the Automobile model so that we can use that data everytime we are referencing a sale.

## Technician Model
The Technicnan model collects the following information:
    first name
    last name
    employee id

To interect with this model in Insomnia, follow the following instructions.

1. List technicians using a "GET" request at this link:
http://localhost:8080/api/technicians/

2. Create a technician using a "POST" request at this link:
http://localhost:8080/api/technicians/

3. Delete a technician using a "DELETE" request at this link:
http://localhost:8080/api/technicians/:id
** It is important to remember to replace ":id" with the id value of the technician you are trying to delete **

## AutomobileVO Model
The AutomobileVO collects the following information:
    vin
    sold

The AutomobileVO model is primarily responsible for corresponding unique VIN values to specific car, helping to determine its sold status, thus determining if the service has a VIP value or not.

## Appointment Model
The Appointment model collects the following information:
    id
    date_time
    reason
    status
    vin
    customer
    technician(ForeignKey)
    vip
    status_choices(allows the appointment to have the status of "created", "cancelled", or "finished")
This model is used for the creation of an appointment, tying in the technician model to assign a valid technician to the ticket.
To interact with this model via Insomnia, follow these inscructions.

1. List appointments using a "GET" request at this url:
http://localhost:8080/api/appointments/

2. Create an appointment using a "POST" request at this url:
http://localhost:8080/api/appointments/

3. Delete a specific appointment using a "DELETE" request at this url:
http://localhost:8080/api/appointments/:id

4. Update an appointment using a "PUT" request at this url:
http://localhost:8080/api/appointments/:id/cancel

5. Update an appointment using a "PUT" request at this url:
http://localhost:8080/api/appointments/:id/finish

## Services Front End
The following is a list of URLS for the react components for the services.
1. http://localhost:3000/technicians/
React file - <TechnicianList.js>
Allows the user to view a list of all technicians.

2. http://localhost:3000/technicians/create/
React file - <CreateNewTechnician.js>
Allows the user to create a new technician.

3. http://localhost:3000/appointments/
React file - <ListAppointment.js>
Allows the user to view a list of appointments, and adjust the status of available appointments.

4. http://localhost:3000/appointments/create/
React file - <CreateAppointment.js>
Allows the user to create a new appointment following given guidelines.

5. http://localhost:3000/appointments/history/
React file - <ServiceHisory.js>
Allows the user to view all appointments, regardless of status.

## Salesperson Model
The Salesperson Model specifically collects the information:
    first name
    last name
    employee id

There are 3 different interactions with the backend of the Salesperson Model, in which we are using Insomnia.

1. List Salespeople (get request, displays a list of all the salespeople)

http://localhost:8090/api/salespeople/

2. Create a Salesperson (post request)

http://localhost:8090/api/salespeople/

3. Delete a Salesperson (delete request)

http://localhost:8090/api/salespeople/:id/


Here is an example of what the json would look like inside the Insomnia "Create a Salesperson" request

{
	"first_name": "Corn",
	"last_name": "Pops",
	"employee_id": 9001
}

After being created the Salesperson will be assigned an ID on the backend. For this example let's say the ID assigned will be 1. To find the ID of the given salesperson, simply send a get request of the list, and the ID of each salesperson will show there.

To delete this salesperson,

One would type inside the URL part of Insomnia

http://localhost:8000/api/salespeople/1/.


## Customer Model
The Customer Model specifically collects the information:
    first name
    last name
    address
    phone number

1. List Customers (get request, displays a list of all the customers)

http://localhost:8090/api/customers/

2. Create a Customer (post request)

http://localhost:8090/api/customers/

3. Delete a Customer (delete request)

http://localhost:8090/api/customers/:id/


Here is an example of what the json would look like inside the Insomnia "Create a Customer" request

{
	"first_name": "Ash",
	"last_name": "Ketchup",
	"employee_id": 151
}

After being created the Customer will be assigned an ID on the backend. For this example let's say the ID assigned will be 1. To find the ID of the given customer, simply send a get request of the list, and the ID of each customer will show there.

To delete this customer,

One would type inside the URL part of Insomnia

http://localhost:8000/api/customers/1/.


## Sale Model
The Sale Model specifically collects the information:
    automobile
    salesperson
    customer
    price

1. List Sales(get request, displays a list of all the sales)

http://localhost:8090/api/sales/

2. Create a Sale (post request)

http://localhost:8090/api/sales/

3. Delete a Sale (delete request)

http://localhost:8090/api/sales/:id/

Our Sale model functions a little different because 3 of our properties are foreign keys. Automobile, salesperson, and customer are the foreign keys that we reference using their specific IDs. So in our earlier examples if Corn Pops ID = 1 were to sell a car to Ash Ketchup ID = 1, this is what the json would look like inside the Insomnia "Create a Sale" request may look like.

{
	"automobile": "4Y1SL65848Z411439",
	"salesperson": 1,
	"customer": 1,
    "price": 15000
}

For the automobile property it will function the same as salesperson and customer where you will input the ID(vin) of the automobile you are wishing to sell (further explained in the next section).

To delete this Sale

One would type inside the URL part of Insomnia

http://localhost:8000/api/sales/1/.


## AutomobileVO Model
As mentioned in the Sales Microservice paragraph, we are using a poller to fetch the data from our Inventory microservice for each instance of Automobile. The information recorded is as such:
    href
    vin
    sold = False/True

The vin is used to identify each automobile, in the same way each salesperson/customer had an unique ID. Through this, we are able to update the sold status of our automobile from False to True once it is processed in a post request for a sale, using that vin. Each vehicle is only meant to be sold once (we are not going to get into pre-owned vehicles).
