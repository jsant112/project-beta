from django.db import models


class Salesperson(models.Model):
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    employee_id = models.CharField(max_length=200, unique=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=200, null=True)
    last_name = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    phone_number = models.CharField(max_length=20, null=True)


class AutomobileVO(models.Model):
    href = models.CharField(max_length=255, null=True)
    vin = models.CharField(max_length=17, unique=True, null=True)
    sold = models.BooleanField(default=False)


class Sale(models.Model):
    automobile = models.ForeignKey(AutomobileVO, on_delete=models.CASCADE, null=True)
    salesperson = models.ForeignKey(Salesperson, on_delete=models.CASCADE, null=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True)
    price = models.IntegerField()
