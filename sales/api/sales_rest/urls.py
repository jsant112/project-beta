from django.urls import path

from .views import (
    show_list_salespeople,
    show_salesperson,
    show_list_customers,
    show_customer,
    show_list_sales,
    show_sale
)

urlpatterns = [
    path("salespeople/", show_list_salespeople, name="show_list_salespeople"),
    path("salespeople/<int:pk>", show_salesperson, name="show_salesperson"),
    path("customers/", show_list_customers, name="show_list_customers"),
    path("customers/<int:pk>", show_customer, name="show_customer"),
    path("sales/", show_list_sales, name="show_list_sales"),
    path("sales/<int:pk>", show_sale, name="show_sale")
]
