# Generated by Django 4.2.2 on 2023-07-26 16:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="AutomobileVO",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("vin", models.CharField(max_length=17, unique=True)),
                ("sold", models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name="Technician",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("first_name", models.CharField(max_length=50)),
                ("last_name", models.CharField(max_length=50)),
                ("employee_id", models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name="Appointment",
            fields=[
                ("id", models.BigAutoField(primary_key=True, serialize=False)),
                ("date_time", models.DateTimeField()),
                ("reason", models.TextField()),
                (
                    "status",
                    models.CharField(
                        choices=[
                            ("created", "created"),
                            ("finished", "finished"),
                            ("canceled", "canceled"),
                        ],
                        default="created",
                        max_length=20,
                    ),
                ),
                ("vin", models.CharField(max_length=17)),
                ("customer", models.CharField(max_length=100)),
                (
                    "technician",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        related_name="appointments",
                        to="service_rest.technician",
                    ),
                ),
            ],
        ),
    ]
